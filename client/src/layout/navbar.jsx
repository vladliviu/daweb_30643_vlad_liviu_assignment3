import React from "react";
import "./navbar.css";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import { withNamespaces } from "react-i18next";
import { useHistory } from "react-router-dom";
import { useEffect } from "react";

const navUnregistered = function NavBar({ t }) {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Navbar.Brand href="/">{t("nav.home")}</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/news">{t("nav.news")}</Nav.Link>
          <NavDropdown title="Profiles" id="collasible-nav-dropdown">
            <NavDropdown.Item href="/studentprofile">
              {t("nav.student")}
            </NavDropdown.Item>
            <NavDropdown.Item href="/coordinator">
              {t("nav.teacher")}
            </NavDropdown.Item>
          </NavDropdown>
          <Nav.Link href="/about">{t("nav.about")}</Nav.Link>
          <Nav.Link href="/contact">{t("nav.contact")}</Nav.Link>
        </Nav>
        <Nav className="ml-auto">
          <Nav.Link href="/login">{t("nav.login")}</Nav.Link>
          <Nav.Link href="/register">{t("nav.register")}</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

const navRegistered = function NavBar({ t }) {
  const history = useHistory();

  const logout = (e) => {
    e.preventDefault();
    localStorage.removeItem("userToken");
    localStorage.removeItem("username");
    localStorage.removeItem("userId");
    history.push("/");
    window.location.reload(true);
  };
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Navbar.Brand href="/">{t("nav.home")}</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/news">{t("nav.news")}</Nav.Link>
          <NavDropdown title="Profiles" id="collasible-nav-dropdown">
            <NavDropdown.Item href="/studentprofile">
              {t("nav.student")}
            </NavDropdown.Item>
            <NavDropdown.Item href="/coordinator">
              {t("nav.teacher")}
            </NavDropdown.Item>
          </NavDropdown>
          <Nav.Link href="/about">{t("nav.about")}</Nav.Link>
          <Nav.Link href="/contact">{t("nav.contact")}</Nav.Link>
        </Nav>
        <Nav className="ml-auto">
          <Nav.Link href="/userprofile">{t("nav.profile")}</Nav.Link>
          <Nav.Link href="" onClick={logout}>
            {t("nav.logout")}
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};
export default withNamespaces()(
  localStorage.userToken ? navRegistered : navUnregistered
);
