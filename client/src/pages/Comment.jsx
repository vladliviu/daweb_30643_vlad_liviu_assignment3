import React, { Component } from "react";
import { withNamespaces } from "react-i18next";
import "./Comment.css";
import axios from "axios";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
} from "reactstrap";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";

class Comment extends Component {
  constructor(props) {
    super(props); //since we are extending class Table so we have to use super in order to override Component class constructor
    this.getComments = this.getComments.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmitComment = this.handleSubmitComment.bind(this);

    this.state = {
      //state is by default an object
      comments: [],
      commentInput: "",
    };
  }

  getComments = () => {
    const url = "http://127.0.0.1:8000/api/comments";
    //const url = urlraw + localStorage.userId;
    return axios
      .get(url, {
        headers: {
          Authorization: `Bearer ${localStorage.userToken}`,
        },
      })
      .then((res) => {
        this.setState({
          comments: res.data.data,
        });
        return res;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleInputChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
    console.log(this.state.nameInput);
    console.log(this.state.commentInput);
  }

  handleSubmitComment() {
    if (localStorage.getItem("username") == null) {
      alert("Please login or register first!");
    }
    const newComment = {
      user_id: localStorage.getItem("userId"),
      name: localStorage.getItem("username"),
      detail: this.state.commentInput,
    };
    const url = "http://127.0.0.1:8000/api/comments";
    //const url = urlraw + localStorage.userId;
    return axios
      .post(url, newComment, {
        headers: {
          Authorization: `Bearer ${localStorage.userToken}`,
        },
      })
      .then((res) => {
        console.log(res);
        window.location.reload(true);
        return res;
      })
      .catch((err) => {
        console.log(err);
      });
  }

  componentDidMount() {
    this.getComments();
  }

  renderTableData() {
    return this.state.comments.map((comment, index) => {
      const { id, name, detail, created_at } = comment; //destructuring
      return (
        <Card key={id}>
          <CardBody>
            <CardTitle>By {name}</CardTitle>
            <CardSubtitle>Created at: {created_at}</CardSubtitle>
            <CardText>Comment: {detail}</CardText>
          </CardBody>
        </Card>
      );
    });
  }

  render() {
    if (localStorage.getItem("userToken")) {
      return (
        <div>
          <h1 id="title">Comment section</h1>
          {this.renderTableData()}
          <Form>
            <FormGroup>
              <Label for="exampleText">Comment</Label>
              <Input
                type="textarea"
                name="commentInput"
                id="exampleText"
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <Button onClick={this.handleSubmitComment}>Submit</Button>
          </Form>
        </div>
      );
    } else {
      return (
        <div>
          <h1 id="title">Please login or register to see or post comments</h1>
        </div>
      );
    }
  }
}

export default withNamespaces()(Comment);
