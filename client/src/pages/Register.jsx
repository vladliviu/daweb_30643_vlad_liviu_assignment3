import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { withNamespaces } from "react-i18next";
import NativeSelect from "@material-ui/core/NativeSelect";
import InputBase from "@material-ui/core/InputBase";
import Typography from "@material-ui/core/Typography";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import { register } from "../components/apiRepo";
import { useHistory } from "react-router-dom";
// for the whole contact form error validation needs to be added (on future versions)
const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(6),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function Register({ t }) {
  const classes = useStyles();
  const history = useHistory();
  const [emailValue, setEmailValue] = useState("");
  const [passwordValue, setPasswordValue] = useState("");
  const [passwordcValue, setPasswordcValue] = useState("");
  const [usernameValue, setUsernameValue] = useState("");
  const [hobbyValue, setHobbyValue] = useState("");
  const [avatarValue, setAvatar] = useState("");

  const submitRegister = (e) => {
    e.preventDefault();
    const newUser = {
      name: usernameValue,
      email: emailValue,
      password: passwordValue,
      c_password: passwordcValue,
      hobbies: hobbyValue,
      image: avatarValue,
    };
    register(newUser).then((res) => {
      history.push("/login");
    });
  };

  const handleEmailChange = (e) => {
    setEmailValue(e.target.value);
    console.log(emailValue);
  };

  const handlePasswordChange = (e) => {
    setPasswordValue(e.target.value);
    console.log(passwordValue);
  };

  const handlePasswordcChange = (e) => {
    setPasswordcValue(e.target.value);
    console.log(passwordcValue);
  };
  const handleUsernameChange = (e) => {
    setUsernameValue(e.target.value);
    console.log(usernameValue);
  };
  const handleHobbyChange = (e) => {
    setHobbyValue(e.target.value + " " + hobbyValue);
    console.log(hobbyValue);
  };
  const handleAvatarChange = (e) => {
    setAvatar(e.target.value);
    console.log(avatarValue);
  };

  const BootstrapInput = withStyles((theme) => ({
    root: {
      "label + &": {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: 4,
      position: "relative",
      backgroundColor: theme.palette.background.paper,
      border: "1px solid #ced4da",
      fontSize: 16,
      padding: "10px 26px 10px 12px",
      transition: theme.transitions.create(["border-color", "box-shadow"]),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(","),
      "&:focus": {
        borderRadius: 4,
        borderColor: "#80bdff",
        boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
      },
    },
  }))(InputBase);
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <PersonAddIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {t("nav.register")}
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="username"
                label={t("auth.username")}
                type="text"
                id="username"
                autoComplete="username"
                onChange={handleUsernameChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label={t("auth.email")}
                name="email"
                autoComplete="email"
                onChange={handleEmailChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="passwordc"
                label={t("auth.password")}
                type="password"
                id="passwordc"
                autoComplete="passwordc"
                onChange={handlePasswordChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label={t("auth.passwordc")}
                type="password"
                id="password"
                autoComplete="password"
                onChange={handlePasswordcChange}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl className={classes.margin}>
                <InputLabel htmlFor="demo-customized-select-native">
                  {t("auth.hobbies")}
                </InputLabel>
                <NativeSelect
                  id="demo-customized-select-native"
                  //value={age}
                  onChange={handleHobbyChange}
                  input={<BootstrapInput />}
                >
                  <option aria-label="None" value="" />
                  <option value={"Sport"}>{t("auth.hobby1")}</option>
                  <option value={"Tech"}>{t("auth.hobby2")}</option>
                  <option value={"Cooking"}>{t("auth.hobby3")}</option>
                </NativeSelect>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <input
                accept="image/*"
                className={classes.input}
                style={{ display: "none" }}
                id="raised-button-file"
                onChange={handleAvatarChange}
                type="file"
              />
              <label htmlFor="raised-button-file">
                <Button
                  color="primary"
                  component="span"
                  className={classes.button}
                >
                  {t("auth.image")}
                </Button>
              </label>{" "}
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={submitRegister}
          >
            {t("nav.register")}
          </Button>
          <Grid container justify="flex-end">
            <Grid item></Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
export default withNamespaces()(Register);
